#!/bin/bash
# This script contains functions used to delete S3 buckets using the AWS CLI
# Requirements:
#  - AWS CLI
#  - aws configure (ACCESS_KEY_ID, SECRET_ACCESS_KEY, format=json
#  - jq  (for parsing json responses)

# Intended Use
# Source (include) this script from a parent script. Call the functions as necessary

# Example:
# BUCKETFILE="buckets.txt"
# source ./delete-s3buckets.sh
# echo "Removing S3 Buckets!"
# while read BUCKET; do
# 	echo "Deleting S3 Bucket: $BUCKET"
#   delete-bucket $BUCKET
# done <$BUCKETFILE%

function s3-empty-bucket () {
    bucket=$1
    objects=`aws s3api list-object-versions --bucket $bucket |jq -r '.Versions + .DeleteMarkers'`
    let object_count=`echo $objects |jq 'length'`-1
    echo -ne "\tDeleting object versions ($((object_count+1)))..."
    echo -ne ""
    if [ $object_count -gt -1 ]
    then
        for i in $(seq 0 $object_count)
        do
            object=$(echo $objects | jq -r ".[$i].Key")
            versionId=$(echo $objects | jq -r ".[$i].VersionId")
            aws s3api delete-object --bucket $bucket --key $object --version-id $versionId > /dev/null
            if [[ $? -eq 0 ]]
            then
                echo -n "."
            else
                return 1
            fi
        done
    fi
    echo -e "OK"
}
function s3-check-bucket () {
    bucket=$1
    echo -ne "\tChecking if bucket exists..."
    aws s3 ls $bucket > /dev/null 2>&1
    if [[ -z $bucket || $? -ne 0 ]]
    then
        echo "it does not exist!"
        return 1
    else
        echo -e "OK"
    fi
}
function delete-bucket () {
    bucket=$1
    retries=3
    echo "Bucket: $bucket"
    s3-check-bucket $bucket || return 1
    while true
    do
        s3-empty-bucket $bucket && echo -ne "\tDeleting bucket..." && aws s3 rb s3://$bucket --force > /dev/null && echo -e "OK" && break
        [[ $((--retries)) -eq 0 ]] && return 1
        echo "Retrying $retries more times..."
    done
}